//
//  SettingsTableViewController.swift
//  ARChartsSampleApp
//
//  Created by DEEPAKKANNA S on 27/05/22.
//

import UIKit
import  ARCharts

protocol SettingsDelegate {
    func didUpdateSettings(_ settings: Settings)
}

class SettingsTableViewController: UITableViewController {
    
    var settings: Settings?
    var delegate: SettingsDelegate?
    
    @IBOutlet weak var labelSwitch: UISwitch!
    @IBOutlet weak var seriesLabel: UILabel!
    @IBOutlet weak var indicesLabel: UILabel!
//    @IBOutlet weak var indicesSlider: UISlider!
    @IBOutlet weak var randomDataSetCell: UITableViewCell!
    @IBOutlet weak var dataSetSegmentedControl: UISegmentedControl!
    
    @IBOutlet weak var RowName_TextField: UITextField!
    
    @IBOutlet weak var ColumnName1: UITextField!
    @IBOutlet weak var ColumnName2: UITextField!
    @IBOutlet weak var ColumnName3: UITextField!
    @IBOutlet weak var ColumnName4: UITextField!
    @IBOutlet weak var ColumnName5: UITextField!
    
    @IBOutlet weak var ColumnValue1: UITextField!
    @IBOutlet weak var ColumnValue2: UITextField!
    @IBOutlet weak var ColumnValue3: UITextField!
    @IBOutlet weak var ColumnValue4: UITextField!
    @IBOutlet weak var ColumnValue5: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupControlsState()
    }
    @IBAction func svaeRowName(_ sender: Any) {
        settings?.rowName = RowName_TextField.text ?? "Row Name"
    }
    
    private func setupControlsState() {
        guard let settings = settings else {
            return
        }
        
        labelSwitch.isOn = settings.showLabels
        dataSetSegmentedControl.selectedSegmentIndex = settings.dataSet
        if dataSetSegmentedControl.selectedSegmentIndex != 0 {
            randomDataSetCell.isHidden = true
        } else {
            randomDataSetCell.isHidden = false
        }
    }
    

    @IBAction func Save_ColumnName(_ sender: UITextField) {
        if sender.tag == 0{
            settings?.ColumnNames[sender.tag] = ColumnName1.text ?? "Column 1"
        }
        else if sender.tag == 1{
            settings?.ColumnNames[sender.tag] = ColumnName2.text ?? "Column 2"
        }
        else if sender.tag == 2{
            settings?.ColumnNames[sender.tag] = ColumnName3.text ?? "Column 3"
        }
        else if sender.tag == 3{
            settings?.ColumnNames[sender.tag] = ColumnName4.text ?? "Column 4"
        }
        else{
            settings?.ColumnNames[sender.tag] = ColumnName5.text ?? "Column 5"
        }
    }
    
    @IBAction func Save_ColumnValues(_ sender: UITextField) {
        if sender.tag == 0{
            settings?.ColumnValues[sender.tag] = Double(ColumnValue1.text ?? "0" ) ?? 0
        }
        else if sender.tag == 1{
            settings?.ColumnValues[sender.tag] = Double(ColumnValue2.text ?? "0" ) ?? 0
        }
        else if sender.tag == 2{
            settings?.ColumnValues[sender.tag] = Double(ColumnValue3.text ?? "0" ) ?? 0
        }
        else if sender.tag == 3{
            settings?.ColumnValues[sender.tag] = Double(ColumnValue4.text ?? "0" ) ?? 0
        }
        else{
            settings?.ColumnValues[sender.tag] = Double(ColumnValue5.text ?? "0" ) ?? 0
        }
    }
    
    
    // MARK: Actions
    
    @IBAction func handleSwitchValueChange(_ sender: UISwitch) {
        settings?.showLabels = sender.isOn
    }
    
    @IBAction func handleDataSetValueChange(_ sender: UISegmentedControl) {
        settings?.dataSet = sender.selectedSegmentIndex
        if sender.selectedSegmentIndex == 0 {
            randomDataSetCell.isHidden = false
        } else {
            randomDataSetCell.isHidden = true
        }
    }
    
    
    @IBAction func handleSeriesSliderValueChanged(_ sender: UISlider) {
        seriesLabel.text = "Row: \(Int(sender.value))"
        settings?.numberOfSeries = Int(sender.value)
    }
    
    @IBAction func handleIndicesSliderValueChanged(_ sender: UISlider) {
        indicesLabel.text = "Column: \(Int(sender.value))"
        settings?.numberOfIndices = Int(sender.value)
    }
    
  

}
